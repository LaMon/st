const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#080607", /* black   */
  [1] = "#8F3F2E", /* red     */
  [2] = "#778479", /* green   */
  [3] = "#A68B73", /* yellow  */
  [4] = "#56758E", /* blue    */
  [5] = "#936D5A", /* magenta */
  [6] = "#77898E", /* cyan    */
  [7] = "#c6c4bf", /* white   */

  /* 8 bright colors */
  [8] = "#080607", /* black   */
  [9] = "#8F3F2E", /* red     */
  [10] = "#778479", /* green   */
  [11] = "#A68B73", /* yellow  */
  [12] = "#56758E", /* blue    */
  [13] = "#936D5A", /* magenta */
  [14] = "#77898E", /* cyan    */
  [15] = "#c6c4bf", /* white   */

  /* special colors */
  [256] = "#080607", /* background */
  [257] = "#c6c4bf", /* foreground */
  [258] = "#c6c4bf", /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
